/*
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include "ui_settingsdialog.h"
#include "modelwrapper.h"

//#define NO_ARCHIVE_MSG "Archive folder is empty!!"
#define TITLE_SETTINGS_DIALOG "Settings"
#define DEFAULT_PLOT_HEIGHT 120
#define ALL_LABEL "All"
#define NO_ARCHIVE_MSG "ERROR: No archive found in "
#define SELECTED_NOT_ARCHIVE_MSG "ERROR: Selected file is not an archive "

class SettingsDialog : public QDialog, public Ui::Settings
{
    Q_OBJECT

public:
    SettingsDialog( QWidget * parent = 0);
    ~SettingsDialog(){}

    /**
    * @brief RestoreSettingsState
    * Restore the last accepted settings state
    */
    void RestoreSettingsState();

    /**
    * @brief IsSettingsStateAccepted
    */
    inline bool IsSettingsStateAccepted()
    {
        return this->settingsStateAccepted;
    }

protected:
    /**
    * @brief eventFilter
    * Custom event filter to monitor the focus of the
    * line edit of the archive folder. It is necessary to display
    * the dialog for the selection of the folder in case the
    * user doesn't want the default one
    */
    bool eventFilter(QObject *obj, QEvent *ev);

private:

    /**
    * @brief SetupDialog
    * Populate archive list and select the default one
    * and construct the TableView for the selection of the
    * modules
    */
    void SetupDialog();

    /**
    * @brief SaveSettingsState
    * Handler called each time the settings dialog is shown to
    * temporarily save the settings state, in order to be able
    * to restore it in case of non-acceptance
    */
    void SaveSettingsState();

    /**
    * @brief BuildModulesModel
    * Build the modules model and inject it in the static model
    * @param modules List of all the modules present in the
    * selected archive
    * @return List of modules in the modules model
    */
    QStringList BuildModulesModel(QStringList modules);

    bbque::ModelWrapper *modelWrapper;

    QString state_folder;
    int state_archive;
    QString state_height;
    bbque::ModelWrapper *state_modelWrapper;

    bool settingsStateAccepted;

private slots:

    /**
    * @brief SwitchArchiveCall
    * Handler called when the user changes the archive in
    * the settings dialog. The static model as well as the modules
    * model is updated
    */
    void SwitchArchiveCall(const QString &);

    /**
    * @brief ModuleChanged
    * Handler called when the user checks or unchecks a module in the
    * settings dialog for updating the list of selected modules and
    * the TableView.
    */
    void ModuleChanged(const QModelIndex&, const QModelIndex&);

    /**
    * @brief OkButtonPressed
    * Handler called when the user completes the settings
    * procedure. The dynamic_model_clean, and both the dynamic
    * models filtered are updated according to te settings. Finally
    * the plots are built.
    */
    void OkButtonPressed();

    /**
    * @brief RejectState
    * Reject the stored state
    */
    void RejectState();

    /**
    * @brief HiddenStartup
    * Handler called when the user selects an empty archive folder
    * and therefore is notified of the error. This is necessary
    * as a workaround for avoiding the program to end after the
    * error message is presented.
    */
    void HiddenStartup();

    /**
    * @brief SelectAllToggled
    * Handler called when the radio box "Select All" is toggled
    */
    void SelectAllClicked();

signals:
    /**
    * @brief SettingsAccepted
    * Signal called at the end of the function OkButtonPressed();
    * to inform MainWindow that the dialog has been closed AFTER the settings
    * have been applied
    * @param integer plot height
    * @param bool hidden mainwindow
    */
    void SettingsAccepted(int, bool);
};

#endif // SETTINGSDIALOG_H
