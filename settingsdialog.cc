#include <QDir>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>

#include "settingsdialog.h"

bool SettingsDialog::eventFilter(QObject* object, QEvent* event)
{
    if(object == lineEdit_folder && event->type() == QEvent::MouseButtonPress)
    {
        QString dir = QFileDialog::getExistingDirectory(this,
                tr("Select Archive Folder"), lineEdit_folder->text(),
                QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

        if(dir != "")
        {
            lineEdit_folder->setText(dir + '/');
            SetupDialog();
            HiddenStartup();
        }

        return false; // lets the event continue to the edit
    }

    return false;
}

SettingsDialog::SettingsDialog( QWidget * parent) : QDialog(parent)
{
    setupUi(this);
    setWindowTitle(QString(TITLE_SETTINGS_DIALOG));
    setWindowFlags(Qt::Dialog | Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    modelWrapper = bbque::ModelWrapper::GetInstance();
    lineEdit_folder->setText(ARCHIVE_FOLDER);
    lineEdit_folder->installEventFilter(this);
    settingsStateAccepted = false;
    SetupDialog();
}

void SettingsDialog::SetupDialog(){
    //Get archive list and set as default archive the one built as last
    QDir dir(lineEdit_folder->text());
    dir.setSorting(QDir::Reversed);
    QStringList archive_list = dir.entryList( QDir::Files,
                                              QDir::Name | QDir::Reversed);

    bool oldState = comboBox_archive->blockSignals(true);
    comboBox_archive->clear();
    comboBox_archive->addItems(archive_list);
    comboBox_archive->blockSignals(oldState);

    if(archive_list.size() > 0)
    {
        std::string default_archive = archive_list.first().toStdString();

        QRegExp rx("bbque-events_*.txt");
        rx.setPatternSyntax(QRegExp::Wildcard);
        if(rx.exactMatch(archive_list.first()))
        {
            modelWrapper->RefreshStaticModel(lineEdit_folder->text().toStdString(),
                                           default_archive);

            QStringList modules = modelWrapper->GetDistinctModules();

            //Populate list of selectable modules according to the default archive
            QStringList selected_modules = BuildModulesModel(modules);

            //link handler checkbox modules
            connect(modelWrapper->GetStaticModel()->GetModulesModel(),
                    SIGNAL(dataChanged ( const QModelIndex&, const QModelIndex&)),
                    this, SLOT(ModuleChanged(const QModelIndex&, const QModelIndex&)));

            //link handler to radio button "Select All"
            connect(radioButton_all, SIGNAL(clicked()), this, SLOT(SelectAllClicked()));

            modelWrapper->GetStaticModel()->SetSelectedModules(selected_modules);
            tableView_module->setModel(modelWrapper->GetStaticModel()->GetModulesModel());

            connect(comboBox_archive, SIGNAL(currentIndexChanged(const QString&)),
                    this, SLOT(SwitchArchiveCall(const QString&)));

            connect(buttonBox, SIGNAL(accepted()), this, SLOT(OkButtonPressed()));

            connect(buttonBox, SIGNAL(rejected()), this, SLOT(RejectState()));

            //Set default value for plot height
            const QString h = QString::number(DEFAULT_PLOT_HEIGHT);
            lineEdit_height->setText(h);

            buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
        }
        else //default file is not an archive
        {
            //empty list of modules
            QStringList modules;

            //Populate list of selectable modules according to the default archive
            QStringList selected_modules = BuildModulesModel(modules);

            modelWrapper->GetStaticModel()->SetSelectedModules(selected_modules);

            tableView_module->setModel(modelWrapper->GetStaticModel()->GetModulesModel());

            buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

            QMessageBox msgBox;
            QString errorMsg = SELECTED_NOT_ARCHIVE_MSG;
            msgBox.setStandardButtons(QMessageBox::Ok);
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.setText(errorMsg);
            msgBox.exec();
        }
    }
    else // no archives
    {
        //empty list of modules
        QStringList modules;

        //Populate list of selectable modules according to the default archive
        QStringList selected_modules = BuildModulesModel(modules);

        modelWrapper->GetStaticModel()->SetSelectedModules(selected_modules);

        tableView_module->setModel(modelWrapper->GetStaticModel()->GetModulesModel());

        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

        QMessageBox msgBox;
        QString errorMsg = NO_ARCHIVE_MSG;
        errorMsg.append(lineEdit_folder->text());
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setText(errorMsg);
        msgBox.exec();
    }

    //adjust TableView horizontal filling
    for(int c = 0; c < tableView_module->horizontalHeader()->count(); ++c)
    {
        tableView_module->horizontalHeader()->setSectionResizeMode(c,
                                                    QHeaderView::Stretch);
    }
}

QStringList SettingsDialog::BuildModulesModel(QStringList modules)
{
    QStringList result;

    //Initialize modules model (three columns: module name,
    //value range from, value range to)
    const int numRows = modules.count();
    const int numColumns = 3;

    modelWrapper->GetStaticModel()->SetModulesModel(
                new QStandardItemModel(numRows, numColumns));
    modelWrapper->GetStaticModel()->SetModelsCount(modules.count());

    int r = 0;

    //Build the model
    QStringList::const_iterator constIterator;
    for (constIterator = modules.constBegin();
         constIterator != modules.constEnd(); ++constIterator)
    {
        QStandardItem* item_name;

        item_name = new QStandardItem(*constIterator);

        item_name->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled);
        item_name->setData(Qt::Checked, Qt::CheckStateRole);

        //set module name
        modelWrapper->GetStaticModel()->GetModulesModel()->setItem(r,
                                                        0, item_name);

        result << *constIterator;

        QStandardItem* item_range_from;

        //set the lower range value to 0 in case ti is higher than 0
        //because otherwise the plots don't start from 0 and don't
        //have much sense
        item_range_from = new QStandardItem(QString::number(
                modelWrapper->GetStaticModel()->GetLowestValue() <= 0 ?
                modelWrapper->GetStaticModel()->GetLowestValue() : 0));

        item_range_from->setFlags(Qt::ItemIsEnabled | Qt::ItemIsEditable);

        //set value range from
        modelWrapper->GetStaticModel()->GetModulesModel()->setItem(r,
                                                    1, item_range_from);

        QStandardItem* item_range_to;

        //get highest value among all the events in the model
        item_range_to = new QStandardItem(QString::number(
                        modelWrapper->GetStaticModel()->GetHighestValue()));

        item_range_to->setFlags(Qt::ItemIsEnabled | Qt::ItemIsEditable);

        //set value range to
        modelWrapper->GetStaticModel()->GetModulesModel()->setItem(r,
                                                    2, item_range_to);

        r++;
    }

    return result;
}

void SettingsDialog::SwitchArchiveCall(const QString& selectedItem)
{
    QRegExp rx("bbque-events_*.txt");
    rx.setPatternSyntax(QRegExp::Wildcard);
    if(rx.exactMatch(selectedItem))
    {
        //load events of the new selected archive
        modelWrapper->RefreshStaticModel(lineEdit_folder->text().toStdString(), selectedItem.toStdString());

        QStringList modules = modelWrapper->GetDistinctModules();

        //rebuild the modules model to present in the Modules TableView
        //according to the new archive
        QStringList selected_modules = BuildModulesModel(modules);

        connect(modelWrapper->GetStaticModel()->GetModulesModel(),
                SIGNAL(dataChanged ( const QModelIndex&,
                const QModelIndex&)), this,
                SLOT(ModuleChanged(const QModelIndex&,
                const QModelIndex&)));

        //refresh list of selected modules
        modelWrapper->GetStaticModel()->SetSelectedModules(selected_modules);

        //refresh Modules TableView
        tableView_module->setModel(
                    modelWrapper->GetStaticModel()->GetModulesModel());

        tableView_module->repaint();

        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
    else //selected file is not an archive
    {
        //empty list of modules
        QStringList modules;

        //Populate list of selectable modules according to the default archive
        QStringList selected_modules = BuildModulesModel(modules);

        modelWrapper->GetStaticModel()->SetSelectedModules(selected_modules);

        tableView_module->setModel(modelWrapper->GetStaticModel()->GetModulesModel());

        buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);

        QMessageBox msgBox;
        QString errorMsg = SELECTED_NOT_ARCHIVE_MSG;
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.setText(errorMsg);
        msgBox.exec();

        HiddenStartup();
    }
}

void SettingsDialog::OkButtonPressed()
{
    modelWrapper->GetStaticModel()->SortSelectedModules();

    //clone static model...dynamic model clean used as a base point
    //for rebuilding the dynamic model when filters are changed
    modelWrapper->SetDynamicModelClean(modelWrapper->GetStaticModel());

    //removed "All" from the list of modules...it will be added again later
    modelWrapper->GetDynamicModelClean()->RemoveItemFromSelectedModules(
                ALL_LABEL);

    //clean dynamic model clean on the basis of the choices made by the
    //user in the settings dialog
    modelWrapper->GetDynamicModelClean()->RemoveUnselected();

    modelWrapper->GetDynamicModelClean()->PopulateStatistics();

    //clone dynamic model clean... the two dynamic model are the one from
    //which data are retrieved for plotting and presenting in the list.
    //they change each time a filter in the corresponding tab is changed.
    modelWrapper->SetDynamicModelFilteredModule(
                modelWrapper->GetDynamicModelClean());

    modelWrapper->SetDynamicModelFilteredType(
                modelWrapper->GetDynamicModelClean());
    //emit signal catched by FillUiWithData() slot in mainwindow to push the plot height
    int plotHeight = abs(lineEdit_height->text().toInt());
    if(plotHeight==0)
        plotHeight = DEFAULT_PLOT_HEIGHT;
    settingsStateAccepted = true;
    SaveSettingsState();
    emit SettingsAccepted(plotHeight, false);
}

void SettingsDialog::RejectState()
{
    settingsStateAccepted = false;
}

void SettingsDialog::HiddenStartup()
{
    modelWrapper->GetStaticModel()->SortSelectedModules();

    //clone static model...dynamic model clean used as a base point
    //for rebuilding the dynamic model when filters are changed
    modelWrapper->SetDynamicModelClean(modelWrapper->GetStaticModel());

    //removed "All" from the list of modules...it will be added again later
    modelWrapper->GetDynamicModelClean()->RemoveItemFromSelectedModules(
                ALL_LABEL);

    //clean dynamic model clean on the basis of the choices made by the
    //user in the settings dialog
    modelWrapper->GetDynamicModelClean()->RemoveUnselected();

    modelWrapper->GetDynamicModelClean()->PopulateStatistics();

    //clone dynamic model clean... the two dynamic model are the one from
    //which data are retrieved for plotting and presenting in the list.
    //they change each time a filter in the corresponding tab is changed.
    modelWrapper->SetDynamicModelFilteredModule(
                modelWrapper->GetDynamicModelClean());

    modelWrapper->SetDynamicModelFilteredType(
                modelWrapper->GetDynamicModelClean());
    //emit signal catched by FillUiWithData() slot in mainwindow to push the plot height
    int plotHeight = abs(lineEdit_height->text().toInt());
    if(plotHeight==0)
        plotHeight = DEFAULT_PLOT_HEIGHT;
    emit SettingsAccepted(plotHeight, true);
}

void SettingsDialog::ModuleChanged(const QModelIndex& topLeft,
                               const QModelIndex& bottomRight)
{
    (void)bottomRight;

    //get modules model items
    QList<QStandardItem*> items =
            modelWrapper->GetStaticModel()->GetModelItems();

    //find specific item changed
    QStandardItem* item = items[topLeft.row()];

    QString m = item->text();

    //update selected modules
    if(item->checkState() == Qt::Unchecked)
    {
        modelWrapper->GetStaticModel()->RemoveSelectedModule(m);

        //temporarily disable signals for the radio button
        //in order to manually update it without generating
        //any signal
        bool oldState =
            radioButton_all->blockSignals(true);
        radioButton_all->setChecked(false);
        radioButton_all->blockSignals(oldState);
    }
    else if(item->checkState() == Qt::Checked)
    {
        bool all = true;

        for(int i = 0; i < items.size(); i++)
        {
            if(items[i]->checkState() != Qt::Checked)
            {
                all = false;
                break;
            }
        }

        if(all == true)
        {
            //temporarily disable signals for the radio button
            //in order to manually update it without generating
            //any signal
            bool oldState =
                radioButton_all->blockSignals(true);
            radioButton_all->setChecked(true);
            radioButton_all->blockSignals(oldState);
        }

        modelWrapper->GetStaticModel()->InsertSelectedModule(m);
    }
}

void SettingsDialog::SelectAllClicked()
{
    QList<QStandardItem*> items =
            modelWrapper->GetStaticModel()->GetModelItems();

    if(radioButton_all->isChecked())
    {
        for(int i = 0; i < items.size(); i++)
        {
            items[i]->setCheckState(Qt::Checked);
            modelWrapper->GetStaticModel()->InsertSelectedModule(
                        items[i]->text());
        }
    }
    else
    {
        for(int i = 0; i < items.size(); i++)
        {
            items[i]->setCheckState(Qt::Unchecked);
            modelWrapper->GetStaticModel()->RemoveSelectedModule(
                        items[i]->text());
        }
    }
}

void SettingsDialog::SaveSettingsState()
{
    if(settingsStateAccepted)
    {
        state_folder = lineEdit_folder->text();
        state_archive = comboBox_archive->currentIndex();
        state_height = lineEdit_height->text();
        state_modelWrapper = modelWrapper->clone();
    }
}

void SettingsDialog::RestoreSettingsState()
{
    lineEdit_folder->setText(state_folder);
    comboBox_archive->setCurrentIndex(state_archive);
    lineEdit_height->setText(state_height);
    *modelWrapper = *state_modelWrapper;

    connect(modelWrapper->GetStaticModel()->GetModulesModel(),
            SIGNAL(dataChanged ( const QModelIndex&, const QModelIndex&)),
            this, SLOT(ModuleChanged(const QModelIndex&, const QModelIndex&)));

    //refresh Modules TableView
    tableView_module->setModel(
                modelWrapper->GetStaticModel()->GetModulesModel());

    tableView_module->repaint();

    QList<QStandardItem*> items =
            modelWrapper->GetStaticModel()->GetModelItems();

    bool all = true;

    for(int i = 0; i < items.size(); i++)
    {
        if(items[i]->checkState() != Qt::Checked)
        {
            all = false;
            break;
        }
    }

    //temporarily disable signals for the radio button
    //in order to manually restore its correct status
    //without generating any signal
    if(all == true)
    {
        bool oldState =
            radioButton_all->blockSignals(true);
        radioButton_all->setChecked(true);
        radioButton_all->blockSignals(oldState);
    }
    else
    {
        bool oldState =
            radioButton_all->blockSignals(true);
        radioButton_all->setChecked(false);
        radioButton_all->blockSignals(oldState);
    }
}
