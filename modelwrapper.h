/*
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODELWRAPPER_H_
#define MODELWRAPPER_H_

#include "bbque/em/event_manager.h"
#include "model.h"

namespace bbque {

class ModelWrapper
{
public:

    ~ModelWrapper();

    /** @brief clone()
     * This function is called to create clone of the ModelWrapper
    */
    ModelWrapper* clone() const { return new ModelWrapper(*this); }

    /**
    * @brief Copy operator
    */
    ModelWrapper& operator=(const ModelWrapper&);

    /** @brief GetInstance()
     * This function is called to create an instance of the class.
     * Calling the constructor publicly is not allowed. The constructor
     * is private and is only called by this Instance function.
    */
    static ModelWrapper *GetInstance();

    void RefreshStaticModel(std::string path, std::string archive);

    QStringList GetDistinctModules();
    QStringList GetDistinctApplications();
    QStringList GetDistinctResources();
    QStringList GetDistinctTypes();

    bbque::Model *GetStaticModel();
    void SetStaticModel(bbque::Model *model);

    bbque::Model *GetDynamicModelClean();
    void SetDynamicModelClean(bbque::Model *model);

    bbque::Model *GetDynamicModelFilteredModule();
    void SetDynamicModelFilteredModule(bbque::Model *model);

    bbque::Model *GetDynamicModelFilteredType();
    void SetDynamicModelFilteredType(bbque::Model *model);

    void SetDynamicModelFiltered(bbque::Model *to, bbque::Model *from);

    void ApplyFilters(bbque::Model *, QString, QString, QString, QString, QString, QString);


private:
    /**
    * @brief modelWrapperInstance
    * Global static pointer used to ensure a single instance of the class.
    */
    static ModelWrapper *modelWrapperInstance;

    ModelWrapper();

    /**
    * @brief Copy constructor
    */
    ModelWrapper(const ModelWrapper &modelWrapper);

    /**
    * @brief static_model
    * Static model built when the archive is selected
    * and maintained as is unless the archive is changed
    */
    bbque::Model *static_model;

    /**
    * @brief dynamic_model_clean
    * Dynamic model built when the OK button in the
    * settings dialog is pressed. It is used to have a
    * semi-static base model from which reconstruct the
    * dynamic models for reflecting the filters
    */
    bbque::Model *dynamic_model_clean;

    /**
    * @brief dynamic_model_filtered_module
    * Dynamic model updated each time a filter in the
    * module tab is changed
    */
    bbque::Model *dynamic_model_filtered_module;

    /**
    * @brief dynamic_model_filtered_type
    * Dynamic model updated each time a filter in the
    * type tab is changed
    */
    bbque::Model *dynamic_model_filtered_type;

};

} // namespace bbque
#endif // MODELWRAPPER_H_
