/*
 * Copyright (C) 2015  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef EXPORTDIALOG_H
#define EXPORTDIALOG_H

#include <QDialog>
#include "ui_export_pdf_dialog.h"
#include "qcpdocumentobject.h"

#define LBL_MAX_PLOT_SIZE "Plot cannot get bigger than (depends on main window size):  "
#define LBL_PAPER_SIZE "Paper size is [pixel]:  "

#define ACTUAL_PLOT_WIDTH 0
#define ACTUAL_PLOT_HEIGHT 0
#define FILENAME_DEFAULT "plot"
#define FILTER_ANY_FILE "Any files (*)"
#define WINDOW_TITLE "Export PDF preview"

class ExportPdfDialog : public QDialog, public Ui::ExportDialog
{
    Q_OBJECT

public:
    /**
    * @brief ExportPdfDialog
    * Constructor that sets up all the main components such as:
    * printer, formatOptions, UI components, event handlers.
    * @param parent pointer to the parent widget.
    */
    ExportPdfDialog(QWidget *parent = 0);

    /**
    * @brief Open
    * Shows the export dialog and sets up all the components.
    * @param plot pointer to the plot to be exported.
    */
    void Open(QCustomPlot *plot);

private:
    /**
    * @brief InitializeDocument
    * Initialize the QCPDocumentObject used to
    * vectorize the plot as image.
    */
    void InitializeDocument();
    /**
    * @brief PrepareDocument
    * Prepares the final document injecting the plot
    * with the choosen sizes in the textEdit.
    */
    void PrepareDocument();
    /**
    * @brief UpdatePlotSizeLabel
    * Updates the label with the initial plot size [pixel]
    */
    void UpdatePlotSizeLabel();
    /**
    * @brief UpdatePaperSizeLabel
    * Updates the label with the paper size [pixel]
    * depending on the choosen format (A2, A3, A4).
    */
    void UpdatePaperSizeLabel();
    /**
    * @brief CalcScaledSize
    * Calculate the size of the plot, scaling it
    * and using the whole width of the page.
    * @param pageSize the size of the page [pixel]
    * @return the scaled size.
    */
    QSize CalcScaledSize(QSize pageSize);
    /**
    * @brief plot
    * Pointer to the QCustomPlot to be exported.
    */
    QCustomPlot *plot;
    /**
    * @brief printer
    * QPrinter object used to print the context of the textEdit.
    */
    QPrinter *printer;
    /**
    * @brief formatOptions
    * QMap use to map the format name
    * with the corresponding PageSize format.
    */
    QMap<QString, QPagedPaintDevice::PageSize> *formatOptions;

private slots:
    /**
    * @brief FormatTypeChanged
    * Handler called to update the textEdit preview
    * when choosing a different page format.
    */
    void FormatTypeChanged();
    /**
    * @brief OrientationChanged
    * Handler caller to update the textEdit preview
    * when orientation changes from
    * portrait mode to landscape mode or viceversa.
    */
    void OrientationChanged();
    /**
    * @brief AutoAdaptaptionChanged
    * Handler caller to update the UI enabling or disabling
    * other UI components used to customize the plot size.
    */
    void AutoAdaptaptionChanged();
    /**
    * @brief SaveButtonPressed
    * Handler called to open the save dialog
    * when user presses the save button.
    * It uses the printer object to print the content of the textEdit.
    */
    void SaveButtonPressed();
    /**
    * @brief CustomSizeChanged
    * Handler called when user selects a custom width or height
    * to scale appropriately the other dimension.
    * It also updates the document in textEdit preview.
    */
    void CustomSizeChanged();
    /**
    * @brief CancelButtonPressed
    * Prints a message for the Cancel operation.
    */
    void CancelButtonPressed();
};

#endif // EXPORTDIALOG_H
