#include "export_image_dialog.h"

ExportImageDialog::ExportImageDialog(QCustomPlot *plot, ExportMode mode)
{
    this->plot = plot;
    this->mode = mode;
    ExportImage();
}

void ExportImageDialog::ExportImage()
{
    // Setup the dialog title
    QString title = "Save as ";
    switch (mode) {
    case 1:
        title.append(JPG_IMAGE);
        break;
    case 2:
        title.append(PNG_IMAGE);
        break;
    default:
        title.append("IMAGE");
        break;
    }
    // Setup the export filename
    QString fileName = QFileDialog::getSaveFileName(this, title,
                                                QString(FILENAME_DEFAULT), FILTER_ANY_FILE);
    if (fileName.isEmpty())
        fileName.append(FILENAME_DEFAULT);
    // Export the image file
    switch (mode) {
    case 1:
        plot->saveJpg(fileName, 0, 0);
        break;
    case 2:
        plot->savePng(fileName, 0, 0);
    default:
        break;
    }


}

ExportImageDialog::~ExportImageDialog()
{

}
