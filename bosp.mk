
ifdef CONFIG_EVENT_VIEWER

# Targets provided by this project
.PHONY: event-viewer clean_event-viewer

# Add this to the "external" target
external: event-viewer
clean_external: clean_event-viewer

MODULE_DIR_EVENT_VIEWER=external/tools/event-viewer

BUILD_TYPE=`echo $(CMAKE_COMMON_OPTIONS) | grep -o "BUILD_TYPE=[a-zA-Z]\{1,\}" | sed -e s/BUILD_TYPE=//g`

.PHONY: event-viewer clean_event-viewer
event-viewer: bbque setup $(BUILD_DIR)/bin/event-viewer
$(BUILD_DIR)/bin/event-viewer:
	@echo
	@echo "==== Building Event Viewer Tool v1.0 ===="
	@cd $(MODULE_DIR_EVENT_VIEWER) && \
		qmake BUILD_DIR=$(BUILD_DIR) BUILD_TYPE=$(BUILD_TYPE) event-viewer.pro && \
		make && \
		make install
	@echo

clean_event-viewer:
	@echo "==== Clean-up Event Viewer Tool v1.0 ===="
	@cd $(MODULE_DIR_EVENT_VIEWER) && \
		make distclean
	@rm -f $(BUILD_DIR)/bin/event-viewer
	@echo

else # CONFIG_EVENT_VIEWER

event-viewer:
	$(warning external Event Viewer module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_EVENT_VIEWER

