#include "export_pdf_dialog.h"
#include <iostream>

using std::cout;
using std::endl;

ExportPdfDialog::ExportPdfDialog(QWidget *parent) : QDialog(parent)
{
    setupUi(this);
    setWindowTitle(WINDOW_TITLE);

    formatOptions = new QMap<QString, QPagedPaintDevice::PageSize>;
    formatOptions->insert("A2",QPrinter::A2);
    formatOptions->insert("A3",QPrinter::A3);
    formatOptions->insert("A4",QPrinter::A4);
    formatOptions->insert("Custom (same as plot size, cannot save additional text)", QPrinter::Custom);

    cbFormat->addItems(QStringList(formatOptions->keys()));
    cbFormat->setCurrentIndex(2);

    printer = new QPrinter();
    printer->setFullPage(true); //to use the defined QPrinter sizes
    printer->setOutputFormat(QPrinter::PdfFormat);
    printer->setPaperSize(formatOptions->value(cbFormat->currentText()));
    printer->setMargins(QPagedPaintDevice::Margins{0,0,0,0});

    connect(buttonBox, SIGNAL(accepted()), this, SLOT(SaveButtonPressed()));
    connect(cbFormat, SIGNAL(currentIndexChanged(int)), this, SLOT(FormatTypeChanged()));
    connect(cbLandscape, SIGNAL(toggled(bool)), this, SLOT(OrientationChanged()));
    connect(cbAdaptToFormat, SIGNAL(toggled(bool)), this, SLOT(AutoAdaptaptionChanged()));
    connect(sbWidth, SIGNAL(valueChanged(int)), this, SLOT(CustomSizeChanged()));
    connect(sbHeight, SIGNAL(valueChanged(int)), this, SLOT(CustomSizeChanged()));
//    connect(cbKeepRatio, SIGNAL(toggled(bool)), this, SLOT());
}

void ExportPdfDialog::InitializeDocument()
{
    // Register the plot document object
    // (only needed once, no matter how many plots will be in the QTextDocument)
    QCPDocumentObject *plotObjectHandler = new QCPDocumentObject(this);
    textEdit_plot->document()->documentLayout()
            ->registerHandler(QCPDocumentObject::PlotTextFormat, plotObjectHandler);
}

void ExportPdfDialog::PrepareDocument(){
    /**   TRY to resize the textedit to show a real preview of the document
    textEdit_plot->setMinimumSize(QSize(2000,2000));
    textEdit_plot->resize(QSize(2000,2000));
    textEdit_plot->setMinimumSize(printer->paperRect().size());
    textEdit_plot->resize(printer->paperRect().size());
*/
    QTextCursor cursor = textEdit_plot->textCursor();

    // Auto-adaptation: compute height and width wrt the choosen format
    // using the whole page width and mantaining the scaling ratio
    if(cbAdaptToFormat->isChecked())
    {
        QSize scaledSize = CalcScaledSize(printer->pageRect().size());
        sbWidth->setValue(scaledSize.width());
        sbHeight->setValue(scaledSize.height());
    }

    textEdit_plot->clear();

    // Insert plot at the cursor position.
    // QCPDocumentObject::generatePlotFormat creates a
    // vectorized snapshot of the plot (with the specified width and height)
    // which gets inserted into the text document.
    cursor.insertText(QString(QChar::ObjectReplacementCharacter),
                          QCPDocumentObject::generatePlotFormat(
                              plot, sbWidth->value(), sbHeight->value()));

    //textEdit_plot->resize(printer->paperRect().size());
    //textEdit_plot->resize(QSize(100,100));
    textEdit_plot->document()->setPageSize(printer->paperRect().size());
    textEdit_plot->updateGeometry();
    textEdit_plot->setTextCursor(cursor);
}

void ExportPdfDialog::SaveButtonPressed()
{
    // qApp->applicationDirPath()
    QString fileName = QFileDialog::getSaveFileName(this, "Saving as PDF document",
                                                    QString(FILENAME_DEFAULT),
                                                    FILTER_ANY_FILE);
    if (fileName.isEmpty())
        fileName.append(FILENAME_DEFAULT);
    if(cbFormat->currentText().contains( QString("Custom")))
        plot->savePdf(fileName, true, sbWidth->value(), sbHeight->value());
    else
    {
        printer->setOutputFileName(fileName);
        cout<< "Printer resolution: " << printer->resolution() << endl;
        cout<< "Page pixel Width: "<< printer->pageRect().width()
            << " Page pixel Height: "<< printer->pageRect().height()<<endl;
        textEdit_plot->document()->setPageSize(QSizeF(printer->pageRect().size()));
        textEdit_plot->document()->print(printer);
    }
}

void ExportPdfDialog::UpdatePlotSizeLabel()
{
    QString maxPlotSize = LBL_MAX_PLOT_SIZE;
    maxPlotSize.append(QString::number(plot->size().width()));
    maxPlotSize.append(" x ");
    maxPlotSize.append(QString::number(plot->size().height()));
    lblMaxPlotSize->setText(maxPlotSize);
}

void ExportPdfDialog::UpdatePaperSizeLabel()
{
    QString paperSize = LBL_PAPER_SIZE;
    paperSize.append(QString::number(printer->paperRect().width()));
    paperSize.append(" x ");
    paperSize.append(QString::number(printer->paperRect().height()));
    lblPaperSize->setText(paperSize);
}

QSize ExportPdfDialog::CalcScaledSize(QSize size)
{
    // Scale the plot size using the whole page width
    // and scaling the height according to the width
    if(size.width() >= plot->width())
        size.setWidth(plot->width());
    if(size.width() >= printer->pageRect().width())
        size.setWidth(printer->pageRect().width());
    double scalingFactor = double(size.width()) / plot->width();
    size.setHeight( scalingFactor * plot->height() );

    return size;
}
void ExportPdfDialog::AutoAdaptaptionChanged()
{
    // Enable/disable appropriately the UI components
    bool flag = !cbAdaptToFormat->isChecked();
    lblMaxPlotSize->setEnabled(flag);
    lblPaperSize->setEnabled(flag);
    lblSize->setEnabled(flag);
    lblWidth->setEnabled(flag);
    lblHeight->setEnabled(flag);
    cbKeepRatio->setEnabled(flag);
    sbWidth->setEnabled(flag);
    sbHeight->setEnabled(flag);
    if(cbAdaptToFormat->isChecked())
    {   // Restore correct size
        QSize scaledSize = CalcScaledSize(printer->pageRect().size());
        sbWidth->setValue(scaledSize.width());
        sbHeight->setValue(scaledSize.height());
    }
}
void ExportPdfDialog::OrientationChanged()
{
    if(cbLandscape->isChecked())
        printer->setOrientation(QPrinter::Landscape);
    else
        printer->setOrientation(QPrinter::Portrait);
    UpdatePaperSizeLabel();
    PrepareDocument();
}
void ExportPdfDialog::FormatTypeChanged()
{
    printer->setPaperSize(formatOptions->value(cbFormat->currentText()));
    // Logic to enable/disable UI components on Custom format
    if(cbFormat->currentText().contains("Custom"))
    {
        cbLandscape->setEnabled(false);
        cbAdaptToFormat->setEnabled(false);
        // Trick to reuse AutoAdaptationChanged
        cbAdaptToFormat->setChecked(true);
        AutoAdaptaptionChanged();
    }
    else
    {
        cbLandscape->setEnabled(true);
        cbAdaptToFormat->setEnabled(true);
    }
    UpdatePaperSizeLabel();
    PrepareDocument();
}
void ExportPdfDialog::CustomSizeChanged()
{
    QString senderName = sender()->objectName();
    // Block signal to avoid strange behaviours
    sbWidth->blockSignals(true);
    sbHeight->blockSignals(true);

    if(cbKeepRatio->isChecked()) // Set W and H mantaining w/h ratio
    {
        if(senderName==sbWidth->objectName())
        {
            QSize size = CalcScaledSize(QSize(sbWidth->text().toInt(), 0));
            sbWidth->setValue(size.width());
            sbHeight->setValue(size.height());
        }
        else // Sender is sbHeight
        {
            if(senderName==sbHeight->objectName())
            {
                // Scale plot width according to the choosen height
                double scalingFactor = double(sbHeight->text().toInt()) / plot->height();
                int calcWidth= scalingFactor*plot->width();
                // Scale width and height according to the page format
                QSize size = CalcScaledSize(QSize(calcWidth, 0));
                sbWidth->setValue(size.width());
                if(calcWidth!=size.width())
                    sbHeight->setValue(size.height());
            }
        }
    }
    else // Force W and H to be lesser than plot size
    {
        if(senderName==sbWidth->objectName() && sbWidth->text().toInt()>plot->width())
            sbWidth->setValue(plot->width());
        else
        if(senderName==sbHeight->objectName() && sbHeight->text().toInt()>plot->height())
            sbHeight->setValue(plot->height());
    }
    sbWidth->blockSignals(false);
    sbHeight->blockSignals(false);
    PrepareDocument();
}
void ExportPdfDialog::CancelButtonPressed()
{
    cout << "CANCEL export operation" << endl;
}

void ExportPdfDialog::Open(QCustomPlot * plot)
{
    this->plot = plot;
    UpdatePlotSizeLabel();
    UpdatePaperSizeLabel();
    InitializeDocument();
    PrepareDocument();
    this->showMaximized();
}
