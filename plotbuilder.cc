#include "plotbuilder.h"

PlotBuilder::PlotBuilder()
{
    modelWrapper = bbque::ModelWrapper::GetInstance();
}

PlotBuilder::~PlotBuilder()
{

}

void PlotBuilder::BuildPlotTypes(QCustomPlot * plot_types, int plotHeight)
{
    // if plotHeight is 0, it means that this function has been called to apply
    // filters, so plotHeight has been defined previously during settings
    if(plotHeight==0)
        plotHeight=this->plotHeight;
    else
        this->plotHeight=plotHeight;

    QStringList types =
            modelWrapper->GetDynamicModelFilteredType()->GetPresentTypes();

    double highest =
            modelWrapper->GetDynamicModelFilteredType()->GetHighestTimestamp();

    double lowest =
            modelWrapper->GetDynamicModelFilteredType()->GetLowestTimestamp();

    //dynamically calculate the padding to place before and after the
    //ticks to plot in order to center the plot
    double graph_padding =
            (double)((highest-lowest)/DYNAMIC_PADDING_PARAMETER);

    QDesktopWidget window;
    int w_x = window.width();

    //clean plot from dirty layers and items to rebuild it from scratch
    plot_types->plotLayout()->clear();
    for(int y = 0; y < plot_types->layerCount(); y++)
    {
        QCPLayer *layer = plot_types->layer(y);

        if(layer->name() == "main")
        {
            while(!layer->children().isEmpty())
            {
                layer->children().last()->setLayer(0);
            }
        }
    }
    plot_types->clearPlottables();

    for(int i = 0; i < types.size(); i++)
    {
        QString type;

        std::vector<std::pair<double, int>> events;

        if(i == 0)
        {
            type = ALL_LABEL;

            //retrieve all events
            events =
                modelWrapper->GetDynamicModelFilteredType()->GetAllEvents();
        }
        else
        {
            type = types[i];

            //retrieve all events of the specific type
            events =
                modelWrapper->GetDynamicModelFilteredType()->GetEventsByType(
                        type);
        }

        QCPAxisRect *plot = new QCPAxisRect(plot_types, false);

        //insert axis rect
        plot_types->plotLayout()->addElement(i, 0, plot);

        //set maximum and minimum sizes for making the subplots sizes
        //unchanged on resize of the splitter.
        plot->setMinimumSize(1, plotHeight);
        plot->setMaximumSize(w_x, plotHeight);

        //setup axes in sub layout axis rects
        plot->addAxes(QCPAxis::atBottom | QCPAxis::atLeft);

        //set number of labels Y axis
        plot->axis(QCPAxis::atLeft)->setAutoTickCount(3);

        //set label of the subplot
        plot->axis(QCPAxis::atLeft)->setLabel(type);

        //set label color
        plot->axis(QCPAxis::atLeft)->setLabelColor(
                    QColor(qSin(i*1+1.2)*80+80, qSin(i*0.3+0)*80+80,
                           qSin(i*0.3+1.5)*80+80));

        //synchronize the left and right margins of
        //the top and bottom axis rects
        QCPMarginGroup *marginGroup = new QCPMarginGroup(plot_types);
        plot->setMarginGroup(QCP::msLeft, marginGroup);

        //move newly created axes on "axes" layer and grids on "grid" layer
        foreach (QCPAxisRect *rect, plot_types->axisRects())
        {
          foreach (QCPAxis *axis, rect->axes())
          {
            axis->setLayer("axes");
            axis->grid()->setLayer("grid");
          }
        }

        //set format labels X axis (timestamps)
        plot->axis(QCPAxis::atBottom)->setNumberFormat("f");
        //set precision labels X axis (timestamps)
        plot->axis(QCPAxis::atBottom)->setNumberPrecision(0);

        plot->axis(QCPAxis::atBottom)->grid()->setVisible(true);
        plot->axis(QCPAxis::atLeft)->grid()->setVisible(true);

        if(i == 0) //all
        {
            for(int k = 1; k < types.size(); k++)
            {
                std::vector<std::pair<double, int>> events_t;
                events_t =
                    modelWrapper->GetDynamicModelFilteredType()->GetEventsByType(
                            types[k]);

                int s_t = events_t.size() + 4;
                QVector<double> x_t(s_t), y_t(s_t);

                int e_t = (int)events_t.size();

                //construct data (adding paddings to center the plot)
                if(e_t > 0)
                {

                    if(e_t == 1)
                    {
                        x_t[2] = events_t[0].first;
                        y_t[2] = (double)events_t[0].second;

                        x_t[4] = lowest - graph_padding;
                        y_t[4] = PADDING_EVENT_VALUE;

                        x_t[3] = lowest;
                        y_t[3] = PADDING_EVENT_VALUE;

                        x_t[1] = highest;
                        y_t[1] = PADDING_EVENT_VALUE;

                        x_t[0] = highest + graph_padding;
                        y_t[0] = PADDING_EVENT_VALUE;

                    }
                    else
                    {
                        for(int w = 0; w < e_t; ++w)
                        {
                            x_t[w+2] = events_t[w].first;
                            y_t[w+2] = (double)events_t[w].second;
                        }

                        x_t[e_t+3] = lowest - graph_padding;
                        y_t[e_t+3] = PADDING_EVENT_VALUE;

                        if(x_t[e_t+1] == lowest)
                        {
                            x_t.removeAt(e_t+2);
                            y_t.removeAt(e_t+2);
                        }
                        else
                        {
                            x_t[e_t+2] = lowest;
                            y_t[e_t+2] = PADDING_EVENT_VALUE;
                        }

                        if(x_t[2] == highest)
                        {
                            x_t.removeAt(1);
                            y_t.removeAt(1);
                        }
                        else
                        {
                            x_t[1] = highest;
                            y_t[1] = PADDING_EVENT_VALUE;
                        }

                        x_t[0] = highest + graph_padding;
                        y_t[0] = PADDING_EVENT_VALUE;
                    }
                }

                //add sub sub graph...the "all" subgraph
                //is composed of as many sub sub graphs as
                //the present types, each one with a different color
                QCPGraph *graph_t = plot_types->addGraph(
                            plot->axis(QCPAxis::atBottom),
                            plot->axis(QCPAxis::atLeft));

                graph_t->setData(x_t, y_t);
                graph_t->setLineStyle(QCPGraph::lsImpulse);
                graph_t->setPen(QPen(
                                    QColor(qSin(k*1+1.2)*80+80,
                                           qSin(k*0.3+0)*80+80,
                                           qSin(k*0.3+1.5)*80+80),
                                    PEN_WIDTH));

                graph_t->setName(types[k]);

                graph_t->rescaleAxes();

                //compute the range of the Y axis on the basis of lowest and
                //highest values in the events
                int lowest_val =
                    modelWrapper->GetDynamicModelFilteredType()->GetLowestValue();

                if(lowest_val > 0)
                {
                   lowest_val = 0;
                }

                QCPRange range = QCPRange(lowest_val,
                    modelWrapper->GetDynamicModelFilteredType()
                                          ->GetHighestValue());

                plot->axis(QCPAxis::atLeft)->setRange(range);
            }
        }
        else //specific type
        {
            int s = events.size() + 4;
            QVector<double> x(s), y(s);

            int e = (int)events.size();

            //construct data (adding paddings to center the plot)
            if(e > 0)
            {
                if(e == 1)
                {
                    x[2] = events[0].first;
                    y[2] = (double)events[0].second;

                    x[4] = lowest - graph_padding;
                    y[4] = PADDING_EVENT_VALUE;

                    x[3] = lowest;
                    y[3] = PADDING_EVENT_VALUE;

                    x[1] = highest;
                    y[1] = PADDING_EVENT_VALUE;

                    x[0] = highest + graph_padding;
                    y[0] = PADDING_EVENT_VALUE;
                }
                else
                {
                    for(unsigned int j = 0; j < events.size(); ++j)
                    {
                        x[j+2] = events[j].first;
                        y[j+2] = (double)events[j].second;
                    }

                    x[e+3] = lowest - graph_padding;
                    y[e+3] = PADDING_EVENT_VALUE;

                    if(x[e+1] == lowest)
                    {
                        x.removeAt(e+2);
                        y.removeAt(e+2);
                    }
                    else
                    {
                        x[e+2] = lowest;
                        y[e+2] = PADDING_EVENT_VALUE;
                    }

                    if(x[2] == highest)
                    {
                        x.removeAt(1);
                        y.removeAt(1);
                    }
                    else
                    {
                        x[1] = highest;
                        y[1] = PADDING_EVENT_VALUE;
                    }

                    x[0] = highest + graph_padding;
                    y[0] = PADDING_EVENT_VALUE;
                }
            }

            //add sub graph
            QCPGraph *graph = plot_types->addGraph(
                plot->axis(QCPAxis::atBottom), plot->axis(QCPAxis::atLeft));

            graph->setData(x, y);
            graph->setLineStyle(QCPGraph::lsImpulse);
            graph->setPen(QPen(QColor(
                qSin(i*1+1.2)*80+80, qSin(i*0.3+0)*80+80,
                qSin(i*0.3+1.5)*80+80), PEN_WIDTH));

            graph->setName(type);

            graph->rescaleAxes();

            //compute the range of the Y axis on the basis of lowest and
            //highest values in the events
            int lowest_val =
                    modelWrapper->GetDynamicModelFilteredType()->GetLowestValue();

            if(lowest_val > 0)
            {
               lowest_val = 0;
            }

            QCPRange range = QCPRange(lowest_val,
                modelWrapper->GetDynamicModelFilteredType()->GetHighestValue());

            plot->axis(QCPAxis::atLeft)->setRange(range);
        }
    }
}

void PlotBuilder::BuildPlotModules(QCustomPlot * plotModules, int plotHeight)
{
    // if plotHeight is 0, it means that this function has been called to apply
    // filters, so plotHeight has been defined previously during settings
    if(plotHeight==0)
        plotHeight=this->plotHeight;
    else
        this->plotHeight=plotHeight;

    QStringList modules =
        modelWrapper->GetDynamicModelFilteredModule()->GetPresentModules();

    double highest =
        modelWrapper->GetDynamicModelFilteredModule()->GetHighestTimestamp();

    double lowest =
        modelWrapper->GetDynamicModelFilteredModule()->GetLowestTimestamp();

    //dynamically calculate the padding to place before and after the
    //ticks to plot in order to center the plot
    double graph_padding =
            (double)((highest-lowest)/DYNAMIC_PADDING_PARAMETER);

    QDesktopWidget window;
    int w_x = window.width();

    //clean plot from dirty layers and items for rebuilding it from scratch
    plotModules->plotLayout()->clear();
    for(int y = 0; y < plotModules->layerCount(); y++)
    {
        QCPLayer *layer = plotModules->layer(y);

        if(layer->name() == "main")
        {
            while(!layer->children().isEmpty())
            {
                layer->children().last()->setLayer(0);
            }
        }
    }
    plotModules->clearPlottables();

    for(int i = 0; i < modules.size(); i++)
    {
        if(i == 0 && !modelWrapper->GetStaticModel()->
                GetSelectedModules().contains(QString(ALL_LABEL)))
        {
         continue;
        }

        QString module;

        std::vector<std::pair<double, int>> events;

        if(i == 0)
        {
            module = ALL_LABEL;

            //retrieve all events
            events =
                modelWrapper->GetDynamicModelFilteredModule()->GetAllEvents();
        }
        else
        {
            module = modules[i];

            //retrieve all events of the specific module
            events =
                modelWrapper->GetDynamicModelFilteredModule()
                    ->GetEventsByModule(module);
        }

        QCPAxisRect *plot = new QCPAxisRect(plotModules, false);

        //insert axis rect
        plotModules->plotLayout()->addElement(i, 0, plot);

        //set maximum and minimum sizes for making the subplots sizes
        //unchanged on resize of the splitter.
        plot->setMinimumSize(1, plotHeight);
        plot->setMaximumSize(w_x, plotHeight);

        //setup axes in sub layout axis rects
        plot->addAxes(QCPAxis::atBottom | QCPAxis::atLeft);

        //set number of labels Y axis
        plot->axis(QCPAxis::atLeft)->setAutoTickCount(3);

        //set label of the subplot
        plot->axis(QCPAxis::atLeft)->setLabel(module);

        //set label color
        plot->axis(QCPAxis::atLeft)->setLabelColor(
                    QColor(qSin(i*1+1.2)*80+80,
                           qSin(i*0.3+0)*80+80,
                           qSin(i*0.3+1.5)*80+80));

        //synchronize the left and right margins of
        //the top and bottom axis rects
        QCPMarginGroup *marginGroup = new QCPMarginGroup(plotModules);
        plot->setMarginGroup(QCP::msLeft, marginGroup);

        //move newly created axes on "axes" layer and grids on "grid" layer
        foreach (QCPAxisRect *rect, plotModules->axisRects())
        {
          foreach (QCPAxis *axis, rect->axes())
          {
            axis->setLayer("axes");
            axis->grid()->setLayer("grid");
          }
        }

        //set format labels X axis (timestamps)
        plot->axis(QCPAxis::atBottom)->setNumberFormat("f");

        //set precision labels X axis (timestamps)
        plot->axis(QCPAxis::atBottom)->setNumberPrecision(0);

        plot->axis(QCPAxis::atBottom)->grid()->setVisible(true);
        plot->axis(QCPAxis::atLeft)->grid()->setVisible(true);

        if(i == 0) //all
        {

            for(int k = 1; k < modules.size(); k++)
            {
                std::pair<double, double> max_values =
                    modelWrapper->GetStaticModel()->GetRangeValuesByModule("All");

                std::vector<std::pair<double, int>> events_t;
                events_t =
                    modelWrapper->GetDynamicModelFilteredModule()
                        ->GetEventsByModule(modules[k]);

                int s_t = events_t.size() + 4;
                QVector<double> x_t(s_t), y_t(s_t);

                int e_t = (int)events_t.size();

                //construct data (adding paddings to center the plot)
                if(e_t > 0)
                {
                    if(e_t == 1)
                    {
                        x_t[2] = events_t[0].first;
                        y_t[2] = (double)events_t[0].second;

                        x_t[4] = lowest - graph_padding;
                        y_t[4] = PADDING_EVENT_VALUE;

                        x_t[3] = lowest;
                        y_t[3] = PADDING_EVENT_VALUE;

                        x_t[1] = highest;
                        y_t[1] = PADDING_EVENT_VALUE;

                        x_t[0] = highest + graph_padding;
                        y_t[0] = PADDING_EVENT_VALUE;

                    }
                    else
                    {
                        for(int w = 0; w < e_t; ++w)
                        {
                            x_t[w+2] = events_t[w].first;
                            y_t[w+2] = (double)events_t[w].second;
                        }

                        x_t[e_t+3] = lowest - graph_padding;
                        y_t[e_t+3] = PADDING_EVENT_VALUE;

                        if(x_t[e_t+1] == lowest)
                        {
                            x_t.removeAt(e_t+2);
                            y_t.removeAt(e_t+2);
                        }
                        else
                        {
                            x_t[e_t+2] = lowest;
                            y_t[e_t+2] = PADDING_EVENT_VALUE;
                        }

                        if(x_t[2] == highest)
                        {
                            x_t.removeAt(1);
                            y_t.removeAt(1);
                        }
                        else
                        {
                            x_t[1] = highest;
                            y_t[1] = PADDING_EVENT_VALUE;
                        }

                        x_t[0] = highest + graph_padding;
                        y_t[0] = PADDING_EVENT_VALUE;
                    }
                }

                //add sub sub graph...the "all" subgraph
                //is composed of as many sub sub graphs as
                //the present modules, each one with a different color
                QCPGraph *graph_t = plotModules->addGraph(
                    plot->axis(QCPAxis::atBottom),
                            plot->axis(QCPAxis::atLeft));

                graph_t->setData(x_t, y_t);
                graph_t->setLineStyle(QCPGraph::lsImpulse);
                graph_t->setPen(QPen(QColor(
                        qSin(k*1+1.2)*80+80,
                        qSin(k*0.3+0)*80+80,
                        qSin(k*0.3+1.5)*80+80), PEN_WIDTH));

                graph_t->setName(modules[k]);

                graph_t->rescaleAxes();

                //compute the range of the Y axis on the basis of the
                //values selected in the settings for the "all" plot
                QCPRange range = QCPRange(max_values.first, max_values.second);
                plot->axis(QCPAxis::atLeft)->setRange(range);
            }
        }
        else //specific module
        {
            std::pair<double, double> max_values =
                modelWrapper->GetStaticModel()->GetRangeValuesByModule(module);

            int s = events.size() + 4;
            QVector<double> x(s), y(s);

            int e = (int)events.size();

            //construct data (adding paddings to center the plot)
            if(e > 0)
            {
                if(e == 1)
                {
                    x[2] = events[0].first;
                    y[2] = (double)events[0].second;

                    x[4] = lowest - graph_padding;
                    y[4] = PADDING_EVENT_VALUE;

                    x[3] = lowest;
                    y[3] = PADDING_EVENT_VALUE;

                    x[1] = highest;
                    y[1] = PADDING_EVENT_VALUE;

                    x[0] = highest + graph_padding;
                    y[0] = PADDING_EVENT_VALUE;
                }
                else
                {
                    for(unsigned int j = 0; j < events.size(); ++j)
                    {
                        x[j+2] = events[j].first;
                        y[j+2] = (double)events[j].second;
                    }

                    x[e+3] = lowest - graph_padding;
                    y[e+3] = PADDING_EVENT_VALUE;

                    if(x[e+1] == lowest)
                    {
                        x.removeAt(e+2);
                        y.removeAt(e+2);
                    }
                    else
                    {
                        x[e+2] = lowest;
                        y[e+2] = PADDING_EVENT_VALUE;
                    }

                    if(x[2] == highest)
                    {
                        x.removeAt(1);
                        y.removeAt(1);
                    }
                    else
                    {
                        x[1] = highest;
                        y[1] = PADDING_EVENT_VALUE;
                    }

                    x[0] = highest + graph_padding;
                    y[0] = PADDING_EVENT_VALUE;
                }
            }

            //add sub graph
            QCPGraph *graph = plotModules->addGraph(
                plot->axis(QCPAxis::atBottom), plot->axis(QCPAxis::atLeft));

            graph->setData(x, y);
            graph->setLineStyle(QCPGraph::lsImpulse);
            graph->setPen(QPen(QColor(qSin(i*1+1.2)*80+80,
                    qSin(i*0.3+0)*80+80,
                    qSin(i*0.3+1.5)*80+80), PEN_WIDTH));

            graph->setName(module);

            graph->rescaleAxes();

            //compute the range of the Y axis on the basis of the
            //values selected in the settings for the specific module
            QCPRange range = QCPRange(max_values.first, max_values.second);
            plot->axis(QCPAxis::atLeft)->setRange(range);
        }
    }
}

